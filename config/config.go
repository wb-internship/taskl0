package config

import (
	"log"
	"os"

	"github.com/joho/godotenv"
)

type DBConfig struct {
	User     string
	Password string
	Driver   string
	Name     string
	Host     string
	Port     string
}

type HTTPConfig struct {
	Port string
}

type StanConfig struct {
	ClusterId    string
	PublisherId  string
	SubscriberId string
}
type Config struct {
	DB   DBConfig
	HTTP HTTPConfig
	Stan StanConfig
}

var config *Config

func NewConfig() *Config {
	err := godotenv.Load()
	if err != nil {
		log.Println("Error loading .env file")
	}

	config = &Config{
		DB: DBConfig{
			User:     os.Getenv("DB_USER"),
			Password: os.Getenv("DB_PASSWORD"),
			Driver:   os.Getenv("DB_DRIVER"),
			Name:     os.Getenv("DB_NAME"),
			Host:     os.Getenv("DB_HOST"),
			Port:     os.Getenv("DB_PORT"),
		},
		HTTP: HTTPConfig{Port: os.Getenv("PORT")},
		Stan: StanConfig{
			ClusterId:    os.Getenv("CLUSTER_ID"),
			PublisherId:  os.Getenv("PUBLISHER_ID"),
			SubscriberId: os.Getenv("SUBSCRIBER_ID"),
		},
	}

	return config
}
