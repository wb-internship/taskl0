package controller

import (
	"github.com/labstack/echo/v4"
	"gorm.io/gorm"
	"net/http"
)

type Controller struct {
	pgsql *gorm.DB
	cache *map[string]string
}

func NewController(pgsql *gorm.DB, cache *map[string]string) *Controller {
	return &Controller{pgsql: pgsql, cache: cache}
}

func (c Controller) GetOrderIndex(ctx echo.Context) error {
	return ctx.File("./src/templates/order_index.html")
}

func (c Controller) GetOrder(ctx echo.Context) error {
	orderId := ctx.Param("id")
	cache := *c.cache
	if cache[orderId] != "" {
		return ctx.String(http.StatusOK, cache[orderId])
	} else {
		return ctx.NoContent(http.StatusNotFound)
	}
}

func ConfigureRoutes(server *echo.Echo, pgsql *gorm.DB, cache *map[string]string) {
	server.GET("/", func(c echo.Context) error {
		return c.JSON(http.StatusOK, map[string]interface{}{
			"hello": "world",
		})
	})

	controller := NewController(pgsql, cache)
	server.GET("/order/", controller.GetOrderIndex)
	server.GET("/order/:id", controller.GetOrder)
}
