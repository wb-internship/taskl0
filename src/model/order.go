package model

type Order struct {
	UID  string `gorm:"primaryKey; column:uid"`
	Data string `gorm:"not null"`
}
