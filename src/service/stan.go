package service

import (
	"encoding/json"
	"fmt"
	"gorm.io/gorm"
	"strings"
	"taskL0/src/dto"
	"taskL0/src/model"
)

type StanService struct {
	pgsql *gorm.DB
	cache *map[string]string
}

func NewStanService(pgsql *gorm.DB, cache *map[string]string) *StanService {
	return &StanService{pgsql: pgsql, cache: cache}
}

func (s StanService) SaveOrder(data []byte) error {
	var order dto.Order
	err := json.Unmarshal(data, &order)
	if err != nil {
		return fmt.Errorf("validation error: %s", err.Error())
	}

	newOrder := model.Order{
		UID:  order.OrderUID,
		Data: string(data),
	}

	if result := s.pgsql.Create(&newOrder); result.Error != nil && strings.Contains(result.Error.Error(), "duplicate key value violates unique") {
		return fmt.Errorf("order with the same uid already existed")
	} else if result.Error != nil {
		return result.Error
	}

	cache := *s.cache
	cache[order.OrderUID] = string(data)

	return nil
}
