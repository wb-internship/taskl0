package dto

import "time"

type Order struct {
	OrderUID    string `json:"order_uid"`
	TrackNumber string `json:"track_number"`
	Entry       string `json:"entry"`
	Delivery    struct {
		Name    string `json:"name"`
		Phone   string `json:"phone"`
		Zip     string `json:"zip"`
		City    string `json:"city"`
		Address string `json:"address"`
		Region  string `json:"region"`
		Email   string `json:"email"`
	} `json:"delivery"`
	Payment struct {
		Transaction  string `json:"transaction"`
		RequestID    string `json:"request_id"`
		Currency     string `json:"currency"`
		Provider     string `json:"provider"`
		Amount       int    `json:"amount"`
		PaymentDt    int    `json:"payment_dt"`
		Bank         string `json:"bank"`
		DeliveryCost int    `json:"delivery_cost"`
		GoodsTotal   int    `json:"goods_total"`
		CustomFee    int    `json:"custom_fee"`
	} `json:"payment"`
	Items []struct {
		ChrtID      int    `json:"chrt_id"`
		TrackNumber string `json:"track_number"`
		Price       int    `json:"price"`
		Rid         string `json:"rid"`
		Name        string `json:"name"`
		Sale        int    `json:"sale"`
		Size        string `json:"size"`
		TotalPrice  int    `json:"total_price"`
		NmID        int    `json:"nm_id"`
		Brand       string `json:"brand"`
		Status      int    `json:"status"`
	} `json:"items"`
	Locale            string    `json:"locale"`
	InternalSignature string    `json:"internal_signature"`
	CustomerID        string    `json:"customer_id"`
	DeliveryService   string    `json:"delivery_service"`
	Shardkey          string    `json:"shardkey"`
	SmID              int       `json:"sm_id"`
	DateCreated       time.Time `json:"date_created"`
	OofShard          string    `json:"oof_shard"`
}

func FakeFactory(uid string) Order {
	return Order{
		OrderUID:    uid,
		TrackNumber: "TRACK123456",
		Entry:       "Test entry",
		Delivery: struct {
			Name    string `json:"name"`
			Phone   string `json:"phone"`
			Zip     string `json:"zip"`
			City    string `json:"city"`
			Address string `json:"address"`
			Region  string `json:"region"`
			Email   string `json:"email"`
		}{
			Name:    "John Doe",
			Phone:   "123-456-7890",
			Zip:     "12345",
			City:    "City",
			Address: "Address",
			Region:  "Region",
			Email:   "john@example.com",
		},
		Payment: struct {
			Transaction  string `json:"transaction"`
			RequestID    string `json:"request_id"`
			Currency     string `json:"currency"`
			Provider     string `json:"provider"`
			Amount       int    `json:"amount"`
			PaymentDt    int    `json:"payment_dt"`
			Bank         string `json:"bank"`
			DeliveryCost int    `json:"delivery_cost"`
			GoodsTotal   int    `json:"goods_total"`
			CustomFee    int    `json:"custom_fee"`
		}{
			Transaction:  "TX1234567890",
			RequestID:    "REQ987654321",
			Currency:     "USD",
			Provider:     "PayPal",
			Amount:       100,
			PaymentDt:    1634731200,
			Bank:         "Bank",
			DeliveryCost: 10,
			GoodsTotal:   90,
			CustomFee:    5,
		},
		Items: []struct {
			ChrtID      int    `json:"chrt_id"`
			TrackNumber string `json:"track_number"`
			Price       int    `json:"price"`
			Rid         string `json:"rid"`
			Name        string `json:"name"`
			Sale        int    `json:"sale"`
			Size        string `json:"size"`
			TotalPrice  int    `json:"total_price"`
			NmID        int    `json:"nm_id"`
			Brand       string `json:"brand"`
			Status      int    `json:"status"`
		}{
			{
				ChrtID:      1,
				TrackNumber: "TRACK123456",
				Price:       50,
				Rid:         "RID123456",
				Name:        "Product 1",
				Sale:        0,
				Size:        "M",
				TotalPrice:  50,
				NmID:        1001,
				Brand:       "Brand A",
				Status:      1,
			},
			{
				ChrtID:      2,
				TrackNumber: "TRACK789012",
				Price:       40,
				Rid:         "RID789012",
				Name:        "Product 2",
				Sale:        10,
				Size:        "L",
				TotalPrice:  36,
				NmID:        1002,
				Brand:       "Brand B",
				Status:      1,
			},
		},
		Locale:            "en_US",
		InternalSignature: "SIG1234567890",
		CustomerID:        "CUST987654321",
		DeliveryService:   "Standard",
		Shardkey:          "SHARDKEY123",
		SmID:              123,
		DateCreated:       time.Now(),
		OofShard:          "OOF_SHARD",
	}
}
