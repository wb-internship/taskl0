package main

import (
	"taskL0"
	"taskL0/config"
)

func main() {
	cfg := config.NewConfig()
	taskL0.Start(cfg)
}
