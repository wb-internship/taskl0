package main

import (
	"encoding/json"
	"github.com/nats-io/stan.go"
	"log"
	"math/rand"
	"taskL0/config"
	"taskL0/src/dto"
)

func main() {
	cfg := config.NewConfig()
	sc, err := stan.Connect(cfg.Stan.ClusterId, cfg.Stan.PublisherId)
	if err != nil {
		log.Fatalln(err)
	}
	defer sc.Close()

	for i := 0; i < 1000; i++ {
		order, err := json.Marshal(dto.FakeFactory(RandStringRunes(5)))
		if err != nil {
			log.Fatalln(err)
		}

		sc.Publish("order", order)
	}
}

func RandStringRunes(n int) string {
	var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}
