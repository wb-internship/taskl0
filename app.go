package taskL0

import (
	"fmt"
	"github.com/labstack/echo/v4"
	"github.com/nats-io/stan.go"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"log"
	"taskL0/config"
	"taskL0/src/controller"
	"taskL0/src/model"
	"taskL0/src/service"
)

func Start(cfg *config.Config) {
	//подключение к бд
	pgsql := DatabaseInit(cfg)

	// создание переменной для кеша
	var cache map[string]string
	cache = make(map[string]string)
	setCache(pgsql, &cache) //заполняем кеш из бд

	// подключение к nats-streaming
	sc, err := stan.Connect(cfg.Stan.ClusterId, cfg.Stan.SubscriberId)
	if err != nil {
		log.Fatalln(err)
	}
	defer sc.Close()
	//подписка на канал "order"
	stanService := service.NewStanService(pgsql, &cache)
	sub, err := sc.Subscribe("order", func(m *stan.Msg) {
		if err := stanService.SaveOrder(m.Data); err != nil {
			log.Printf("Couldnt save message: %s", err)
		}
	}, stan.DeliverAllAvailable())
	defer sub.Close()

	//запуск сервера
	server := echo.New()
	controller.ConfigureRoutes(server, pgsql, &cache)
	server.Logger.Fatal(server.Start(":8080"))
}

func DatabaseInit(cfg *config.Config) *gorm.DB {
	dsn := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s sslmode=disable TimeZone=Asia/Jakarta", cfg.DB.Host, cfg.DB.User, cfg.DB.Password, cfg.DB.Name, cfg.DB.Port)
	database, e := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if e != nil {
		panic(e)
	}

	if err := database.AutoMigrate(model.Order{}); err != nil {
		log.Fatalf("can't migrate db: %s\n", err)
		return nil
	}

	return database
}

func setCache(pgsql *gorm.DB, cache *map[string]string) {
	var orders []model.Order
	if result := pgsql.Find(&orders); result.Error != nil {
		log.Fatal(result.Error)
	}

	for _, order := range orders {
		(*cache)[order.UID] = order.Data
	}
}
